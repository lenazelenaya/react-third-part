package com.chatapp.backend.message;

import com.chatapp.backend.message.dto.MessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/messages")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @GetMapping
    public List<MessageDTO> getAllMessages() {
        return messageService.getAllMessages();
    }

    @GetMapping("/{id}")
    public Optional<MessageDTO> getMessageById(@PathVariable UUID id) {
        return messageService.getMessageById(id);
    }

    @PostMapping
    public MessageDTO updateMessage(@RequestBody MessageDTO newMessage) {
        return messageService.saveMessage(newMessage);
    }

}
