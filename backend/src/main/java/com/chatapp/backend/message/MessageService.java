package com.chatapp.backend.message;

import com.chatapp.backend.message.dto.MessageDTO;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageService {

    private Map<UUID, MessageDTO> messages = null;

    public List<MessageDTO> getAllMessages() {
        return messages == null ? initMessages() : new ArrayList<>(messages.values());
    }

    public Optional<MessageDTO> getMessageById(UUID id) {
        if (messages == null) {
            initMessages();
        }
        if (!messages.containsKey(id)) {
            return Optional.empty();
        }
        return Optional.of(messages.get(id));
    }

    public MessageDTO saveMessage(MessageDTO message) {
        if (message.id == null) {
            message.id = UUID.randomUUID();
        }
        messages.put(message.id, message);
        return message;
    }


    public List<MessageDTO> initMessages() {
        messages = new HashMap<>();
        UUID uuid = UUID.fromString("80e03257-1b8f-11e8-9629-c7eca82aa7bd");
        var message_1 = new MessageDTO(uuid, "I don’t *** understand. It is the Panama accounts", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-13T19:48:12.936Z", "");
        var message_3 = new MessageDTO(uuid, "Tells exactly what happened.", UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "2020-07-16T19:48:42.481Z", "2020-07-13T19:48:47.481Z");
        var message_4 = new MessageDTO(uuid, "You were doing your daily bank transfers and…", UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "2020-07-14T19:48:56.273Z", "");
        var message_5 = new MessageDTO(uuid, "Yes, like I’ve been doing every *** day without red *** flag", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-14T19:49:14.480Z", "");
        var message_6 = new MessageDTO(uuid, "There is never been a *** problem.", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-14T19:48:28.769Z", "");
        var message_7 = new MessageDTO(uuid, "Why this account?", UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "2020-07-14T19:49:33.195Z", "");
        var message_8 = new MessageDTO(uuid, "I do not *** know! I do not know!", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-15T19:49:45.821Z", "");
        var message_9 = new MessageDTO(uuid, "What the ** is a red flag anyway?", UUID.fromString("5328dba1-1b8f-11e8-9629-c7eca82aa7bd"), "Ben", "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg", "2020-07-15T19:50:07.708Z", "");
        var message_10 = new MessageDTO(uuid, "You said you could handle things", UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "2020-07-15T19:53:02.483Z", "");
        var message_11 = new MessageDTO(uuid, "I did what he taught me.", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:53:17.272Z", "2020-07-15T19:53:50.272Z");
        var message_12 = new MessageDTO(uuid, "it is not my fucking fault!", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:53:49.171Z", "");
        var message_13 = new MessageDTO(uuid, "Can you fix this? Can you fix it?", UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "2020-07-16T19:56:51.491Z", "");
        var message_14 = new MessageDTO(uuid, "Her best is gonna get us all killed.", UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "2020-07-16T19:57:07.965Z", "2020-07-16T19:57:15.965Z");
        var message_15 = new MessageDTO(uuid, "I do not know how!", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:58:06.686Z", "");
        var message_16 = new MessageDTO(uuid, "it means that the accounts frozen that cause the feds might think that there is a crime being committed.", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:52:04.375Z", "");
        var message_17 = new MessageDTO(uuid, "Like by me", UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "2020-07-16T19:52:15.334Z", "");
        var message_18 = new MessageDTO(uuid, "aaaha!", UUID.fromString("5328dba1-1b8f-11e8-9629-c7eca82aa7bd"), "Ben", "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg", "2020-07-16T19:58:17.878Z", "");
        putMessages(uuid, message_1, message_3, message_4, message_5, message_6, message_7, message_8, message_9);
        putMessages(uuid, message_10, message_11, message_12, message_13, message_14, message_15, message_16, message_17, message_18);
        return new ArrayList<>(messages.values());
    }

    private void putMessages(UUID uuid, MessageDTO ... messagesDTO) {
        for(MessageDTO message: messagesDTO){
            messages.put(uuid, message);
        }
    }

}
