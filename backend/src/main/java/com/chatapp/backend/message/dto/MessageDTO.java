package com.chatapp.backend.message.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class MessageDTO {
    public UUID id;
    public String text;
    public UUID userId;
    public String user;
    public String avatar;
    public String createdAt;
    public String editedAt;

    public MessageDTO(UUID id, String text, UUID userId, String user, String avatar, String createdAt, String editedAt) {
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.user = user;
        this.avatar = avatar;
        this.createdAt = createdAt;
        this.editedAt = editedAt;
    }
}
