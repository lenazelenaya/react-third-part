package com.chatapp.backend.user;

import com.chatapp.backend.user.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService usersService;

    @GetMapping
    public List<UserDTO> getAllUsers() {
        return usersService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserDTO getUserById(@PathVariable UUID id) {
        return usersService.getUserById(id);
    }

    @PostMapping
    public UserDTO updateUser(@RequestBody UserDTO newUser) {
        return usersService.updateUser(newUser);
    }

}
