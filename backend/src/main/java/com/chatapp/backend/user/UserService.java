package com.chatapp.backend.user;

import com.chatapp.backend.page.AuthUser;
import com.chatapp.backend.user.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements UserDetailsService {
    Map<UUID, UserDTO> users;

    public List<UserDTO> generateUsers() {
        users = new HashMap<>();
        users.put(UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), new UserDTO(UUID.fromString("9e243930-83c9-11e9-8e0c-8f1a686f4ce4"), "Ruth", "1", "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA", "user"));
        users.put(UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), new UserDTO(UUID.fromString("533b5230-1b8f-11e8-9629-c7eca82aa7bd"), "Wendy", "1", "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng", "user"));
        users.put(UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), new UserDTO(UUID.fromString("4b003c20-1b8f-11e8-9629-c7eca82aa7bd"), "Helen", "1", "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ", "user"));
        users.put(UUID.fromString("5328dba1-1b8f-11e8-9629-c7eca82aa7bd"), new UserDTO(UUID.fromString("5328dba1-1b8f-11e8-9629-c7eca82aa7bd"), "Ben", "1", "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg", "user"));

        var id = UUID.randomUUID();
        users.put(id, new UserDTO(id, "admin", "admin", "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQ5aHdld6sjfTgD7pRrFba0eRmksYw4QnNmK8L7IbFM2C_BMuvd7tL5IpfmpxhJE4K2lLu34dhmALhLTd4xdH5gMA5PbZW2DURpIzn&usqp=CAU&ec=45682155", "admin"));

        return new ArrayList<>(users.values());
    }

    public List<UserDTO> getAllUsers() {
        return users == null ? generateUsers() : new ArrayList<>(users.values());
    }

    public UserDTO getUserById(UUID id) {
        return users.get(id);
    }

    public UserDTO updateUser(UserDTO newUser) {
        if (newUser.id == null) {
            newUser.id = UUID.randomUUID();
        }
        users.put(newUser.id, newUser);
        return newUser;
    }

    @Override
    public AuthUser loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = users.values()
                .stream()
                .filter(u -> u.getName().equals(username))
                .findFirst().orElseThrow(() -> new UsernameNotFoundException("No user with this name"));
        return new AuthUser(user.getId(), user.getName(), user.getPassword());
    }
}