package com.chatapp.backend.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class UserDTO {
    public UUID id;
    public String name;
    public String password;
    public String avatar;
    public String role;
}
