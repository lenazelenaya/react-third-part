import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Switch, Route, useHistory } from "react-router";

import Chat from "./container/Chat";
import MainHeader from "./components/MainHeader";
import Footer from "./components/Footer";
import Login from "./components/Login/ingex";
import { login, setId, setRole } from "./actions/pageActions";
import ErrorModal from "./components/ErrorPage";
import MessageEditor from "./components/MessageEditor";
import UserEditor from "./components/UserEditor";
import UserList from "./components/UserList";

import "./App.css";

interface AppProps {
  isLogged: boolean;
  userRole: string;
  login: Function;
  setRole: Function;
  setId: Function;
}

function App({ isLogged, login, setId, userRole, setRole }: AppProps) {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.token) {
      const userId = localStorage.id;
      setId(userId);
      const role = localStorage.role;
      setRole(role);
      login();
    }
  }, []);

  return (
    <div className="App">
      <MainHeader />
      <ErrorModal />
      {isLogged && userRole !== "admin" ? (
        <Switch>
          <Route exact path="/" component={Chat} />
          <Route exact path="/edit" component={MessageEditor} />
          <Route path="/edit/:messageId" component={MessageEditor} />
        </Switch>
      ) : null}
      {isLogged && userRole === "admin" ? (
        <Switch>
          <Route exact path="/" component={Chat} />
          <Route exact path="/edit" component={MessageEditor} />
          <Route path="/edit/:messageId" component={MessageEditor} />
          <Route exact path="/userList" component={UserList} />
          <Route exact path="/userEdit" component={UserEditor} />
          <Route path="/userEdit/:userId" component={UserEditor} />
        </Switch>
      ) : null}
      {!isLogged ? (
        <Switch>
          <Route exact path="/" component={Login} />
        </Switch>
      ) : null}
      <Footer />
    </div>
  );
}

interface Store {
  page: {
    isLogged: boolean;
    userRole: string;
  };
}
const mapStateToProps = (state: Store) => {
  return {
    isLogged: state.page.isLogged,
    userRole: state.page.userRole,
  };
};
const mapDispatchToProps = {
  login,
  setId,
  setRole,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
