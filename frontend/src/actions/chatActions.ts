import { ChatAction } from "./chatActionTypes";
import Message from "../types/message";

export const setStorage = (messages: Message[], participants: number) => ({
  type: ChatAction.SET_STORAGE,
  payload: {
    messages,
    participants,
  },
});
export const initStorage = () => ({
  type: ChatAction.INIT_STORAGE,
});

export const addMessage = (data: { userId: string; text: string }) => ({
  type: ChatAction.ADD_MESSAGE,
  payload: {
    data,
  },
});

export const setLike = (
  messageId: string,
  userId: string,
  likesId: string[]
) => ({
  type: ChatAction.SET_LIKE,
  payload: {
    messageId,
    userId,
    likesId,
  },
});

export const editMessage = (messageId: string, text: string) => ({
  type: ChatAction.EDIT_MESSAGE,
  payload: {
    messageId,
    text,
  },
});

export const deleteMessage = (messageId: string) => ({
  type: ChatAction.DELETE_MESSAGE,
  payload: {
    messageId,
  },
});

