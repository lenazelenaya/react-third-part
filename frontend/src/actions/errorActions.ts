import { ErrorAction } from "./errorActionTypes";

export const closeError = () => ({
  type: ErrorAction.CLOSE_ERROR,
});
export const showError = (errorText: string) => ({
  type: ErrorAction.SHOW_ERROR,
  payload: {
    errorText,
  },
});
