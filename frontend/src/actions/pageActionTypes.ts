export enum PageAction {
  LOGIN = "LOGIN",
  LOGOUT = "LOGOUT",
  SHOW_LOADING = "SHOW_LOADING",
  CLOSE_LOADING = "CLOSE_LOADING",
  SET_USER_ROLE = "SET_USER_ROLE",
  SET_ID = "SET_ID",
}
