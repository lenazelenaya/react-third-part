import { PageAction } from "./pageActionTypes";

export const closeLoading = () => ({
  type: PageAction.CLOSE_LOADING,
});

export const showLoading = () => ({
  type: PageAction.SHOW_LOADING,
});

export const login = () => ({
  type: PageAction.LOGIN,
});
export const setRole = (role: string) => ({
  type: PageAction.SET_USER_ROLE,
  payload: {
    type: role,
  },
});
export const logout = () => ({
  type: PageAction.LOGOUT,
});

export const setId = (id: string) => ({
  type: PageAction.SET_ID,
  payload: {
    id,
  },
});
