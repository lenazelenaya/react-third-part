export enum UserAction {
  SET_USERS = "SET_USERS",
  EDIT_USER = "EDIT_USER",
  INIT_USERS = "INIT_STORAGE",
}
