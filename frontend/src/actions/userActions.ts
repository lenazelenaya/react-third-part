import { UserAction } from "./userActionTypes";
import User from "../types/user";

export const initUsers = () => ({
  type: UserAction.INIT_USERS,
});

export const setUsers = (users: User[]) => ({
  type: UserAction.SET_USERS,
  payload: {
    users,
  },
});

export const editUser = (
  userId: string,
  changedProps: {
    avatar: string;
    user: string;
    password: string;
    role: string;
  }
) => ({
  type: UserAction.EDIT_USER,
  payload: {
    changeProps: changedProps,
    userId,
  },
});
