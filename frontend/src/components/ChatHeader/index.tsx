import React from "react";

import "./index.css";

interface HeaderProps {
  name: string;
  participants: number;
  messageCount: number;
  lastMessage: string;
}

export default function ChatHeader({
  name,
  participants,
  messageCount,
  lastMessage,
}: HeaderProps) {
  return (
    <div className="chat-header">
      <div className="chat-name chat-header-div">{name}</div>
      <div className="chat-header-div chat-header-counts">
        <div className="participants-count">
          <span className="text">
            participants: {"      "}
            {participants}
          </span>
        </div>
        <div className="messages-count">
          <span className="text">
            messages: {"       "}
            {messageCount}
          </span>
        </div>
      </div>
      <div className="last-message chat-header-div">
        last message at:{"  "}
        {lastMessage}
      </div>
    </div>
  );
}
