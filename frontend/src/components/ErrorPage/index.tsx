import React from "react";
import { closeError } from "../../actions/errorActions";
import { connect } from "react-redux";

interface ErrorProps {
  isShown: boolean;
  errorMessage: string;
  hideError: Function;
}
function Error({ hideError, isShown, errorMessage }: ErrorProps) {
  const handleClose = () => {
    hideError();
  };
  if (isShown) {
    return (
      <div className="error_modal">
        <div className="error_modal-root">
          <div className="error_modal-header">
            <div>
              <span>Error</span>
            </div>
            <button className="error_modal-close-btn" onClick={handleClose}>
              x
            </button>
          </div>
          <div className="error_modal-body">
            <span>{errorMessage}</span>
          </div>
        </div>
      </div>
    );
  } else return null;
}

interface Store {
  error: {
    isShown: boolean;
    errorMessage: string;
  };
}

const mapStateToProps = (state: Store) => {
  return {
    isShown: state.error.isShown,
    errorMessage: state.error.errorMessage,
  };
};
const mapDispatchToProps = {
  hideError: closeError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Error);
