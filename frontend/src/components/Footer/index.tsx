import React from "react";

import "./index.css";

interface FooterState {}

interface FooterProps {}

export default function Footer() {
  return (
    <footer className="footer">
      <span>© Powered by EZ for BSA homework</span>
    </footer>
  );
}
