import React from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import { History } from "history";

import ps from "../../services/pageService";
import { connect } from "react-redux";
import { login, setRole, setId } from "../../actions/pageActions";

import "./index.css";

interface LoginProps {
  login: Function;
  setId: Function;
  setRole: Function;
  history: History;
}

interface FormValues {
  username: string;
  password: string;
}

function Login({
  login,
  setId,
  setRole,
  history,
}: LoginProps) {
  const defaultValues: FormValues = { username: "", password: "" };
  return (
    <div className="login">
      <Formik
        initialValues={defaultValues}
        onSubmit={async (values, actions) => {
          const response = await ps.login(values);
          if (response.error) {
            if (response.statusCode === 404) {
              actions.setFieldError("username", "Invalid username");
            }
            if (response.statusCode === 401) {
              actions.setFieldError("password", "Invalid password");
            }
          } else {
            login();
            setId(response.data!.id);
            setRole(response.data!.role);
            if (response.data!.role === "admin") history.push("/userList");
          }
        }}
      >
        <Form className="login_form">
          <Field
            className="login_field"
            type="text"
            name="username"
            placeholder="Username"
          />
          <ErrorMessage className="login_error" component="div" name="login" />

          <Field
            className="login_field"
            type="password"
            name="password"
            placeholder="Password"
          />
          <ErrorMessage
            className="login_error"
            component="div"
            name="password"
          />
          <button type="submit" className="login_btn">
            {" "}Submit{" "}
          </button>
        </Form>
      </Formik>
    </div>
  );
}

const mapDispatchToProps = {
  login,
  setId,
  setRole,
};
export default connect(null, mapDispatchToProps)(Login);
