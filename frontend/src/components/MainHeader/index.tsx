import React from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router";

import { logout } from "../../actions/pageActions";

import "./index.css";

interface HeaderProps {
  name: string;
  isLogged: boolean;
  logout: Function;
}

function Header({ name, isLogged, logout }: HeaderProps) {
  const history = useHistory();

  const onLogout = () => {
    logout();
    localStorage.clear();
    history.push("/");
  };

  return (
    <header className="header">
      <span>{name}</span>
      {isLogged ? (
        <button className="page_logout" onClick={onLogout}>
          Logout
        </button>
      ) : null}
    </header>
  );
}

interface Store {
  chat: {
    name: string;
  };
  page: {
    isLogged: boolean;
  };
}
const mapStateToProps = (state: Store) => {
  return {
    name: state.chat.name,
    isLogged: state.page.isLogged,
  };
};
const mapDispatchToProps = {
  logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
