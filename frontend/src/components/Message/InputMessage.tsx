import React from "react";
import { connect } from "react-redux";

import Message from "../../types/message";
import { setLike } from "../../actions/chatActions";

import "./index.css";

interface InputProps {
  message: Message;
  userId: string;
  setLike: Function;
}

interface InputState {
  isLiked: boolean;
}

class InputMessage extends React.Component<InputProps, InputState> {
  constructor(props: InputProps) {
    super(props);
    this.state = {
      isLiked: false,
    };
  }
  componentDidMount() {
    this.props.message.likesId.every((id) => {
      if (id === this.props.userId) {
        this.setState({ isLiked: true });
        return false;
      }
      return true;
    });
  }

  onLike() {
    const likesId = this.props.message.likesId;
    this.props.setLike(this.props.message.id, this.props.userId, likesId);
  }

  render() {
    return (
      <div className="input-message">
        <div className="message-avatar">
          <div className="message-avatar-shadow">
            <img
              className="avatar"
              src={this.props.message.avatar}
              alt="avatar"
            />
          </div>
        </div>
        <div className="message-content">
          <div className="message-meta">
            <span className="message-date">{this.props.message.timeShow}</span>
            <span className="message-author">{this.props.message.user}</span>
          </div>
          <div className="message-text">{this.props.message.text}</div>
          <div className="actions">
            <div className="message-like action" onClick={() => this.onLike()}>
              <span className="like">
                {this.state.isLiked ? "You like this" : "Like?"}
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
interface Store {
  page: {
    userId: string;
  };
}
const mapStateToProps = (state: Store) => {
  return {
    userId: state.page.userId,
  };
};
const mapDispatchToProps = {
  setLike,
};

export default connect(mapStateToProps, mapDispatchToProps)(InputMessage);
