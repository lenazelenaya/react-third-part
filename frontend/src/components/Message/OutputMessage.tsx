import React from "react";
import { connect } from "react-redux";
import { History } from "history";

import Message from "../../types/message";
import { deleteMessage, editMessage } from "../../actions/chatActions";

import "./index.css";

interface OutputProps {
  message: Message;
  history: History;
  deleteMessage: Function;
}

class OutputMessage extends React.Component<OutputProps> {
  constructor(props: OutputProps) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
  }

  handleDelete() {
    this.props.deleteMessage(this.props.message.id);
  }

  onEdit() {
    this.props.history.push(`/edit/${this.props.message.id}`);
  }

  render() {
    const isEdited = this.props.message.editedAt ? 1 : 0;
    return (
      <div className="output-message">
        <div className="message_user-name">{this.props.message.user}</div>
        <div className="message_data">
          <div className="message_text">
            <span> {this.props.message.text}</span>
          </div>
          <div className="message_info">
            <div className="message_date">{this.props.message.timeShow}</div>

            <button className="message_edit-btn" onClick={this.onEdit}>
              Edit
            </button>
            <button
              onClick={() => this.handleDelete()}
              className="message_delete-btn"
            >
              Delete
            </button>
            {isEdited ? <span className="message_edited">edited</span> : null}
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  deleteMessage,
  editMessage,
};

export default connect(null, mapDispatchToProps)(OutputMessage);
