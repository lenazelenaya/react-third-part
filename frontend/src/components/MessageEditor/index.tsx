import React, { useState } from "react";
import { History } from "history";
import { connect } from "react-redux";

import { editMessage } from "../../actions/chatActions";
import Spinner from "../Spinner";
import * as pageAction from "../../actions/pageActions";
import * as errorAction from "../../actions/errorActions";
import * as chatAction from "../../actions/chatActions";

import cs from "../../services/chatService";

import "./index.css";

interface EditProps {
  isLoading: boolean;
  history: History;
  hideLoading: Function;
  showError: Function;
  editMessage: Function;
  match: {
    params: {
      messageId?: string;
    };
  };
}

interface EditState {
  messageId: string;
  text: string;
}

class Edit extends React.Component<EditProps, EditState> {
  constructor(props: EditProps) {
    super(props);
    this.state = {
      text: "",
      messageId: "",
    };

    this.handleEdit = this.handleEdit.bind(this);
    this.handleTyping = this.handleTyping.bind(this);
  }

  async componentDidMount() {
    const messageId = this.props.match.params.messageId;
    if (messageId) {
      this.setState({ messageId: messageId });
      const response = await cs.getMessageText(messageId);
      if (response.error) {
        this.props.showError(response.message);
      } else {
        this.setState({ text: response.text });
      }
    }
    this.props.hideLoading();
  }
  handleTyping(event: React.FormEvent<HTMLInputElement>) {
    this.setState({ text: event.currentTarget.value });
  }

  handleEdit() {
    this.props.editMessage(this.state.messageId, this.state.text);
    this.props.history.push("/");
  }

  onClose() {
    this.props.history.push("/");
  }
  render() {
    if (this.props.isLoading) return <Spinner />;
    return (
      <div className="modal-layer">
        <div className="modal-root">
          <div className="modal-header">
            <span>Edit Message</span>
          </div>
          <div className="modal-body">
            <div className="edit-form">
              <input
                type="text"
                className="edit-text-area"
                value={this.state.text}
                onChange={() => this.handleTyping}
              />
              <button className="edit-btn" onClick={this.handleEdit}>
                Edit
              </button>
              <button className="cancel-btn" onClick={() => this.onClose()}>
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

interface Store {
  page: {
    isLoading: boolean;
  };
}

const mapStateToProps = (state: Store) => {
  return {
    isLoading: state.page.isLoading,
  };
};

const mapDispatchToProps = {
  showLoading: pageAction.showLoading,
  hideLoading: pageAction.closeLoading,
  editMessage: chatAction.editMessage,
  showError: errorAction.showError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);
