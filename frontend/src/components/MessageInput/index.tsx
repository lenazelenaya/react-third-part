import React from "react";
import { connect } from "react-redux";
import { History } from "history";

import { addMessage } from "../../actions/chatActions";
import Message from "../../types/message";

import "./index.css";

interface InputState {
  typeMessage: string;
}

interface InputProps {
  messages: Message[];
  userId: string;
  history: History;
  addMessage: Function;
}

class MessageInput extends React.Component<InputProps, InputState> {
  constructor(props: InputProps) {
    super(props);
    this.state = {
      typeMessage: "",
    };
    this.handleTyping = this.handleTyping.bind(this);
    this.handleKey = this.handleKey.bind(this);
  }

  onSend() {
    this.props.addMessage({
      text: this.state.typeMessage,
      userId: this.props.userId,
    });
  }

  handleKey(event: React.KeyboardEvent) {
    if (event.keyCode === 38) {
      const lastMessage = this.props.messages[this.props.messages.length - 1];
      if (lastMessage.userId === this.props.userId) {
        this.props.history.push(`/edit/${lastMessage.id}`);
      }
    }
  }

  handleTyping(event: React.FormEvent<HTMLInputElement>) {
    this.setState({ typeMessage: event.currentTarget.value });
  }

  render() {
    return (
      <div className="message-input">
        <form className="message-input-area">
          <input
            type="text"
            className="text-area"
            value={this.state.typeMessage}
            onChange={this.handleTyping}
            onKeyDown={this.handleKey}
          />
        </form>
        <button className="message-input-btn" onClick={() => this.onSend()}>
          Send
        </button>
      </div>
    );
  }
}

interface Store {
  chat: {
    messages?: Message[];
  };
  page: {
    userId: string;
  };
}

const mapStateToProps = (state: Store) => {
  return {
    messages: state.chat.messages!,
    userId: state.page.userId,
  };
};

const mapDispatchToProps = {
  addMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
