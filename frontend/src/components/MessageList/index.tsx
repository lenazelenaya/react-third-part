import React from "react";
// @ts-ignore
import { animateScroll } from "react-scroll";
import { History } from "history";
import { connect } from "react-redux";

import Message from "../../types/message";
import InputMessage from "../Message/InputMessage";
import OutputMessage from "../Message/OutputMessage";
import ms from "../../services/messageService";

import "./index.css";

interface ListProps {
  messages: Message[];
  history: History;
  userId: string;
}

class MessageList extends React.Component<ListProps> {
  componentDidMount() {
    animateScroll.scrollToBottom({ containerId: "message-list", duration: 0 });
  }

  getMessage(message: Message, id: string) {
    if (message.userId === this.props.userId) {
      return (
        <OutputMessage
          key={id}
          message={message}
          history={this.props.history}
        />
      );
    } else {
      return (
        <InputMessage message={message} key={id} />
      );
    }
  }

  render() {
    return (
      <div className="message-list" id="list">
        {ms.groupByDate(this.props.messages!).map((groupsByDate, id) => (
          <div className="message-list-group" key={id}>
            <div className="separator">{groupsByDate.date}</div>
            {groupsByDate.messages.map((message: Message, id: string) =>
              this.getMessage(message, id)
            )}
          </div>
        ))}
      </div>
    );
  }
}
interface Store{
  page: {
    userId: string;
  }
}

const mapStateToProps = (state: Store) => {
  return {
    userId: state.page.userId,
  }
}

export default connect(mapStateToProps)(MessageList);
