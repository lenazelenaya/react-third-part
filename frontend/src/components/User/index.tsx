import React from "react";
import { History } from "history";

interface UserProps {
  id: string;
  avatar: string;
  user: string;
  role: string;
  password: string;
  history: History;
}

export default function User({ avatar, user, id, role, password, history }: UserProps) {
  const onEdit = () => {
    history.push(`/userEdit/${id}`);
  };

  return (
    <div className="user_form">
      <div>
        <img src={avatar} alt="avatar" className="user_avatar" />
      </div>
      <div className="user_info-form">
        <span className="user_info">
          <b>Id: </b>
          {id}
        </span>
        <span className="user_info">
          <b>Username: </b>
          {user}
        </span>
        <span className="user_info">
          <b>Role: </b>
          {role}
        </span>
        <span className="user_info">
          <b>Password: </b>
          {password}
        </span>
        <button className="user_info-btn" onClick={onEdit}>
          Edit
        </button>
      </div>
    </div>
  );
}