import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import { connect } from "react-redux";

import { initUsers } from "../../actions/userActions";
import { closeLoading } from "../../actions/pageActions";
import { showError } from "../../actions/errorActions";
import { editUser } from "../../actions/userActions";
import UserService from "../../services/userService";
import Spinner from "../Spinner";

interface UserProps {
  match: {
    params: {
      userId?: string;
    };
  };
  isLoading: boolean;
  hideLoading: Function;
  showError: Function;
  editUser: Function;
  initUsers: Function;
}

function UserEditor({
  match,
  isLoading,
  hideLoading,
  showError,
  editUser,
  initUsers,
}: UserProps) {
  const [values, setValues] = useState({
    avatar: "",
    user: "",
    password: "",
    role: "",
  });

  const history = useHistory();

  useEffect(() => {
    const userId = match.params.userId;

    if (userId) {
      const response = UserService.getUser(userId)
        .then((response) => {
          if (response.error) throw new Error(response.message!);
          setValues({
            avatar: response.data!.avatar,
            role: response.data!.role,
            user: response.data!.user,
            password: response.data!.password,
          });
        })
        .catch((error) => {
          showError(error.message);
        })
        .finally(() => {
          hideLoading();
        });
    }
  }, [match.params.userId]);
  if (isLoading) return <Spinner />;

  return (
    <div className="user_edit-wrapper">
      <Formik
        enableReinitialize={true}
        initialValues={values}
        onSubmit={(values) => {
          if (match.params.userId) editUser(match.params.userId, values);
          history.push("/user_list");
          initUsers();
        }}
      >
        <Form className="user_edit-form">
          <label htmlFor="user">Username</label>
          <Field
            className="user_edit-input"
            type="text"
            name="user"
            placeholder="Username"
          />
          <label htmlFor="avatar">Avatar</label>
          <Field
            className="user_edit-form"
            type="text"
            name="avatar"
            placeholder="Avatar"
          />
          <label htmlFor="username">Role</label>

          <Field
            className="user_edit-form"
            type="text"
            name="role"
            placeholder="role"
          />

          <label htmlFor="password">Password</label>
          <Field
            className="user_edit-form"
            type="text"
            name="password"
            placeholder="Password"
          />
          <button type="submit" className="user_edit-btn">
            Edit
          </button>
        </Form>
      </Formik>
    </div>
  );
}
interface Store {
  page: {
    isLoading: boolean;
  };
}
const mapStateToProps = (state: Store) => {
  return {
    isLoading: state.page.isLoading,
  };
};
const mapDispatchToProps = {
  hideLoading: closeLoading,
  showError,
  editUser,
  initUsers,
};
export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);
