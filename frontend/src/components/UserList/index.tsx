import React, { useEffect } from "react";
import { connect } from "react-redux";
import { History } from "history";

import { initUsers } from "../../actions/userActions";
import UserElement from "../User";
import User from "../../types/user";
import Spinner from "../Spinner";

interface UserProps {
  users: User[];
  isLoading: boolean;
  history: History;
  initUsers: Function;
}
function UserList({ users, isLoading, initUsers, history }: UserProps) {
  useEffect(() => {
    initUsers();
  }, []);
  if (isLoading) return <Spinner />;
  return (
    <div className="userList">
      {users
        ? users.map((user, id) => (
            <UserElement
              key={id}
              id={user.id}
              history={history}
              avatar={user.avatar}
              user={user.user}
              password={user.password}
              role={user.role}
            />
          ))
        : null}
    </div>
  );
}

interface Store {
  page: {
    isLoading: boolean;
  };
  user: {
    users: User[];
  };
}

const mapStateToProps = (state: Store) => {
  return {
    isLoading: state.page.isLoading,
    users: state.user.users,
  };
};

const mapDispatchToProps = {
  initUsers,
};
export default connect(mapStateToProps, mapDispatchToProps)(UserList);
