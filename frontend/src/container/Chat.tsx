import React, { useEffect } from "react";
import { connect } from "react-redux";
import { History } from "history";

import Message from "../types/message";
import MessageList from "../components/MessageList";
import Spinner from "../components/Spinner";
import ChatHeader from "../components/ChatHeader/index";
import MessageInput from "../components/MessageInput/index";
import * as chatAction from "../actions/chatActions";

import "./chat.css";

interface ChatProps {
  isLoading: boolean;
  messages?: Message[];
  participants?: number;
  name: string;
  history: History;
  setStorage: Function;
  hideLoading: Function;
  showLoading: Function;
  addMessage: Function;
}

function Chat({
  history,
  setStorage,
  messages,
  participants,
  name,
  isLoading,
}: ChatProps) {
  useEffect(() => {
    setStorage();
  }, []);

  let lastMessage: string | undefined = "";
  let messageCount = 0;
  if (messages && messages!.length > 0) {
    lastMessage = messages![messages!.length - 1].timeShow;
    messageCount = messages!.length;
  }

  if (isLoading)
    return (
      <div className="chat-wrapper">
        <Spinner />
      </div>
    );
  else
    return (
      <div className="chat-wrapper">
        <ChatHeader
          name={name + "chat"}
          messageCount={messageCount}
          participants={participants!}
          lastMessage={lastMessage!}
        />
        <MessageList messages={messages!} history={history} />
        <MessageInput history={history} />
      </div>
    );
}

interface Store {
  chat: {
    messages?: Message[];
    participants?: number;
    chatName: string;
  };
  page: {
    isLoading: boolean;
  };
}

const mapStateToProps = (state: Store) => {
  return {
    messages: state.chat.messages,
    participants: state.chat.participants,
    chatName: state.chat.chatName,
    isLoading: state.page.isLoading,
  };
};
const mapDispatchToProps = {
  setStorage: chatAction.setStorage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
