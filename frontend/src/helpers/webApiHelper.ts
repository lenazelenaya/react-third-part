import configureStore from "../store";

function getFetchArgs(type: string, body: any | undefined) {
  const headers: HeadersInit = new Headers();
  const token = localStorage.token;
  if (token) {
    headers.set("Authorization", `Bearer ${token}`);
  }
  if (body) {
    headers.set("Content-Type", "application/json");
    headers.set("Accept", "application/json");
  }
  if (body && type === "GET") {
    throw new Error("GET request does not support request body.");
  }
  return {
    method: type,
    mode: 'no-cors' as RequestMode,
    headers,
    body: JSON.stringify(body),
  };
}

export default async function makeRequest(
  type: string,
  url: string,
  body?: any
) {
  const res = await fetch(url, getFetchArgs(type, body));
  return res;
}
