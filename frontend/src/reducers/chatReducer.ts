import { ChatAction } from "../actions/chatActionTypes";
import Message from "../types/message";

interface ReducerState {
  isLoading: boolean;
  messages?: Message[];
  participants?: number;
  messagesNumber?: number;
  name: string;
}

interface Action {
  type: ChatAction;
  payload?: {
    idMessage?: string;
    data?: any;
    participants?: number;
    messages?: Message[];
  };
}

const initialState: ReducerState = {
  isLoading: true,
  messages: [],
  participants: 5,
  name: "LOGO",
};

export default function (state = initialState, action: Action) {
  switch (action.type) {
    case ChatAction.SET_STORAGE: {
      const { messages, participants } = action.payload!;
      return { ...state, messages, participants };
    }
    default:
      return state;
  }
}
