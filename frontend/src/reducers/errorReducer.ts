import { ErrorAction } from "../actions/errorActionTypes";

interface ErrorState {
  errorMessage: string;
  isShown: boolean;
}

interface ErrorsAction {
  type: ErrorAction;
  payload?: {
    errorMessage?: string;
  };
}

const initialState: ErrorState = {
  errorMessage: "",
  isShown: false,
};

export default function (state = initialState, action: ErrorsAction) {
  switch (action.type) {
    case ErrorAction.CLOSE_ERROR: {
      return { ...state, isShown: false };
    }
    case ErrorAction.SHOW_ERROR: {
      const { errorMessage } = action.payload!;
      return { ...state, isShown: true, errorMessage };
    }
    default:
      return state;
  }
}
