import { combineReducers } from "redux";

import page from "./pageReducer";
import chat from "./chatReducer";
import user from "./userReducer"
import error from "./errorReducer"

const rootReducer = combineReducers({
  page,
  chat,
  user,
  error,
});

export default rootReducer;
