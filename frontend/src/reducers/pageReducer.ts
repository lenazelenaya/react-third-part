import { PageAction } from "../actions/pageActionTypes";

interface PageState {
  isLoading: boolean;
  isLogged: boolean;
  userId?: string;
}

interface PagesAction {
  type: PageAction;
  payload?: {
    id: string;
    role: string;
  };
}

const initialState: PageState = {
  isLoading: true,
  isLogged: false,
};

export default function (state = initialState, action: PagesAction) {
  switch (action.type) {
    case PageAction.SHOW_LOADING: {
      return { ...state, isLoading: true };
    }
    case PageAction.CLOSE_LOADING: {
      return { ...state, isLoading: false };
    }
    case PageAction.LOGIN: {
      return { ...state, isLogged: true };
    }
    case PageAction.LOGOUT: {
      return { ...state, isLogged: false };
    }
    case PageAction.SET_USER_ROLE: {
      const { role } = action.payload!;
      return { ...state, userRole: role };
    }
    case PageAction.SET_ID: {
      const { id } = action.payload!;
      return { ...state, userId: id };
    }
    default:
      return state;
  }
}