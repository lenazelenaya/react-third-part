import { UserAction } from "../actions/userActionTypes";
import User from "../types/user";
interface UserState {
  userList?: User[];
}

interface UsersAction {
  type: UserAction;
  payload?: {
    userList: string;
  };
}

const initialState: UserState = {};

export default function (state = initialState, action: UsersAction) {
  switch (action.type) {
    case UserAction.SET_USERS: {
      const { userList } = action.payload!;
      return { ...state, userList };
    }
    default:
      return state;
  }
}