import { ChatAction } from "../actions/chatActionTypes";
import { PageAction } from "../actions/pageActionTypes";
import { ErrorAction } from "../actions/errorActionTypes";
import cs from "../services/chatService";
import { AnyAction } from "redux";
import { call, takeEvery, put, all } from "redux-saga/effects";

export function* addMessage(action: AnyAction) {
  try {
    const userId = action.payload.data.userId;
    const messageText = action.payload.data.text;
    yield put({ type: PageAction.SHOW_LOADING });
    const newMessage = yield call(cs.sendMessage, messageText, userId);
    if (newMessage.error) {
      throw new Error(newMessage.message);
    }
    const messages = yield call(cs.loadData);
    yield put({ type: ChatAction.SET_STORAGE, payload: { ...messages } });
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchAddMessage() {
  yield takeEvery(ChatAction.ADD_MESSAGE, addMessage);
}

export function* deleteMessage(action: AnyAction) {
  try {
    const messageId = action.payload.idMessage;
    yield put({ type: PageAction.SHOW_LOADING });
    const deletedMessage = yield call(cs.deleteMessage, messageId);
    if (deletedMessage.error) {
      throw new Error(deletedMessage.message);
    }
    const messages = yield call(cs.loadData);
    yield put({ type: ChatAction.SET_STORAGE, payload: { ...messages } });
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchDeleteMessage() {
  yield takeEvery(ChatAction.DELETE_MESSAGE, deleteMessage);
}

export function* initChat(action: AnyAction) {
  try {
    yield put({ type: PageAction.SHOW_LOADING });
    const messages = yield call(cs.loadData);
    yield put({ type: ChatAction.SET_STORAGE, payload: { ...messages } });
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchInitChat() {
  yield takeEvery(ChatAction.INIT_STORAGE, initChat);
}

export function* editMessage(action: AnyAction) {
  try {
    const messageId = action.payload.idMessage;
    const newText = action.payload.text;
    yield put({ type: PageAction.SHOW_LOADING });
    const editedMessage = yield call(cs.editMessage, messageId, newText);
    if (editedMessage.error) {
      throw new Error(editedMessage.message);
    }
    const messages = yield call(cs.loadData);
    yield put({ type: ChatAction.SET_STORAGE, payload: { ...messages } });
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchEditMessage() {
  yield takeEvery(ChatAction.EDIT_MESSAGE, editMessage);
}

export function* changeLike(action: AnyAction) {
  try {
    yield put({ type: PageAction.SHOW_LOADING });
    const messageId = action.payload.idMessage;
    const userId = action.payload.userId;
    const likesId = action.payload.likesId;
    const indexUser = likesId.indexOf(userId);
    if (~indexUser) {
      likesId.splice(indexUser, 1);
    } else {
      likesId.push(userId);
    }

    const changedLike = yield call(cs.changeLike, messageId, likesId);
    if (changedLike.error) {
      throw new Error(changedLike.message);
    }
    const messages = yield call(cs.loadData);
    yield put({ type: ChatAction.SET_STORAGE, payload: { ...messages } });
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchChangeLike() {
  yield takeEvery(ChatAction.SET_LIKE, changeLike);
}

export default function* chatSagas() {
  yield all([
    watchAddMessage(),
    watchDeleteMessage(),
    watchInitChat(),
    watchEditMessage(),
    watchChangeLike(),
  ]);
}
