import { AnyAction } from "redux";
import { call, takeEvery, put, all } from "redux-saga/effects";
import { UserAction } from "../actions/userActionTypes";
import { PageAction } from "../actions/pageActionTypes";
import { ErrorAction } from "../actions/errorActionTypes";
import UserService from "../services/userService";

export function* editUser(action: AnyAction) {
  try {
    yield put({ type: PageAction.SHOW_LOADING });
    const editUserProps = action.payload.editProps;
    const userId = action.payload.userId;
    const editedUser = yield call(UserService.editUser, userId, editUserProps);
    if (editedUser.error) {
      throw new Error(editedUser.message);
    }
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchEditUser() {
  yield takeEvery(UserAction.EDIT_USER, editUser);
}

export function* initStorage(action: AnyAction) {
  try {
    yield put({ type: PageAction.SHOW_LOADING });
    const users = yield call(UserService.getUsers);
    yield put({ type: UserAction.SET_USERS, payload: { users } });
  } catch (error) {
    yield put({
      type: ErrorAction.SHOW_ERROR,
      payload: { errorText: error.message },
    });
  } finally {
    yield put({ type: PageAction.CLOSE_LOADING });
  }
}

function* watchInitStorage() {
  yield takeEvery(UserAction.INIT_USERS, initStorage);
}

export default function* userSagas() {
  yield all([watchEditUser(), watchInitStorage()]);
}
