import callWebApi from "../helpers/webApiHelper";
import Message from "../types/message";
import User from "../types/user";
import us from "./userService";

class ChatService {
  constructor() {
    this.loadData = this.loadData.bind(this);
  }
  async getMessages() {
    try {
      const endpoint = "http://localhost:5000/api/messages";
      const type = "GET";
      const response = await callWebApi(type, endpoint);
      let msg = await response.json();
      const users = await us.getUsers();
      const messages: Message[] = msg.data.map((message: any) => {
        message.timeShow = this.getTimeToShow(message.createdAt);
        const user = users.find((user: User) => user.id === message.userId);
        if (user) {
          message.avatar = user!.avatar;
          message.user = user!.user;
        }
        return message;
      });
      messages.sort(this.messageComparator);
      return messages;
    } catch (error) {
      throw error;
    }
  }
  async getMessageText(messageId: string) {
    const apiUrl = `http://localhost:5000/api/messages/${messageId}`;
    const type = "GET";
    const response = await callWebApi(type, apiUrl);
    const rsp = await response.json();
    const isAdmin = localStorage.role === "admin";
    console.log(localStorage.role);
    if (!isAdmin && rsp.data.userId !== localStorage.id) {
      return {
        error: true,
        message: "Not your message",
      };
    }
    return {
      error: false,
      text: rsp.data.text,
    };
  }
  async loadData() {
    const messages = await this.getMessages();
    const participants = this.getCountParticipants(messages);
    return { messages, participants };
  }
  async sendMessage(text: string, userId: string) {
    const endpoint = "http://localhost:5000/api/messages";
    const type = "POST";
    const response = await callWebApi(type, endpoint, { userId, text });
    const rsp = await response.json();
    return rsp;
  }
  async deleteMessage(messageId: string) {
    const endpoint = `http://localhost:5000/api/messages/${messageId}`;
    const type = "DELETE";
    const response = await callWebApi(type, endpoint, {
      id: messageId,
    });
    const rsp = await response.json();
    return rsp;
  }
  getTimeToShow(date: string | Date) {
    const newDate = new Date(date);
    let minutes = newDate.getMinutes().toString();
    if (minutes.length === 1) minutes = "0" + minutes;
    return `${newDate.getHours()}:${minutes}`;
  }

  async editMessage(messageId: string, text: string) {
    const endpoint = `http://localhost:5000/api/messages/${messageId}`;
    const type = "GET";
    const response = await callWebApi(type, endpoint, { text });
    const rsp = await response.json();
    return rsp;
  }
  async changeLike(messageId: string, likesId: string[]) {
    const endpoint = `http://localhost:5000/api/messages/${messageId}`;
    const type = "GET";
    const response = await callWebApi(type, endpoint, { likesId });
    const rsp = await response.json();
    return rsp;
  }

  getCountParticipants(messages: Message[]) {
    const participants = new Set();
    messages.forEach((message: Message) => {
      participants.add(message.userId);
    });
    return participants.size;
  }

  messageComparator(a: Message, b: Message) {
    if (new Date(a.createdAt) > new Date(b.createdAt)) return 1;
    if (new Date(a.createdAt) < new Date(b.createdAt)) return -1;
    return 0;
  }
  getMessageId() {
    return (new Date().getTime() * Math.random()).toString();
  }
}

export default new ChatService();
