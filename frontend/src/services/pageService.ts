import callWebApi from "../helpers/webApiHelper";

class PageService {
  async login({
    username,
    password,
  }: {
    username: string;
    password: string;
  }): Promise<{
    error?: boolean;
    data?: {
      id: string;
      role: string;
    };
    statusCode: number;
    message?: string;
  }> {
    try {
      const endpoint = "http://localhost:5000/api/auth/login";
      const type = "POST";
      const response = await callWebApi(type, endpoint, {
        username,
        password,
      });
      const rspns = await response.json();
      if (rspns.data) {
        localStorage.setItem("id", rspns.data.user.id);
        localStorage.setItem("role", rspns.data.user.role);
      }
      return rspns;
    } catch (error) {
      throw error;
    }
  }
}

export default new PageService();
