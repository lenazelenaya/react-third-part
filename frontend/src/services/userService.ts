import callWebApi from "../helpers/webApiHelper";
import User from "../types/user";

class UserService {
  async getUsers() {
    try {
      const endpoint = "http://localhost:5000/api/users";
      const type = "GET";
      const response = await callWebApi(type, endpoint);
      const rspns = await response.json();
      const users: User[] = rspns.data;
      return users;
    } catch (error) {
      throw error;
    }
  }
  async getUser(
    userId: string
  ): Promise<{
    data?: User;
    message?: string;
    statusCode: number;
    error: boolean;
  }> {
    try {
      const endpoint = `http://localhost:5000/api/users/${userId}`;
      const type = "GET";
      const response = await callWebApi(type, endpoint);
      return await response.json();
    } catch (error) {
      throw error;
    }
  }
  async editUser(
    userId: string,
    changedProps: {
      avatar?: string;
      user?: string;
      password?: string;
      type?: string;
    }
  ) {
    try {
      const endpoint = `http://localhost:5000/api/users/${userId}`;
      const type = "POST";
      const response = await callWebApi(type, endpoint, changedProps);
      return await response.json();
    } catch (error) {
      throw error;
    }
  }
}

export default new UserService();
