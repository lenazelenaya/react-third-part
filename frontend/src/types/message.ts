export default interface Message {
  id: string;
  userId:string;
  user: string;
  text: string;
  avatar?: string;
  createdAt: Date | string;
  editedAt?: Date | string;
  timeShow?: string;
  likesId: string[];
}
